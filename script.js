// Code goes here
var app = angular.module('myApp', []); 
app.controller('todoCtrl', function($scope) {
    $scope.todoList = [{todoText:'Eat', done:false},{todoText:'Movie',done:false}];
    $scope.totaltodos=$scope.todoList.length;

    $scope.todoAdd = function() {
      if($scope.todoInput!=''){
        $scope.todoList.push({todoText:$scope.todoInput, done:false});
        $scope.todoInput = "";
        $scope.totaltodos=$scope.todoList.length;
      }
    };
    
    $scope.remove = function() {
        var oldList = $scope.todoList.filter(function(value){
          if(!value.done)
          return value;
        })
        $scope.todoList=oldList;
        $scope.totaltodos=$scope.todoList.length;
    };
});